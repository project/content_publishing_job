<?php

namespace Drupal\Tests\content_publishing_job\Kernel;

use Drupal\block\Entity\Block;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\KernelTests\KernelTestBase;

/**
 * Kernel test for RelatedContentBlock.
 *
 * @group content_publishing_job
 */
class RelatedContentBlockTest extends KernelTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'node',
    'content_publishing_job',
    'system',
  ];

  /**
   * An entity type manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->entityTypeManager = $this->container->get('entity_type.manager');

    // Create a test block.
    $block = Block::create([
      'id' => 'custom_block_test',
      'label' => $this->t('Custom Block Test'),
      'plugin' => 'related_contents_block',
    ]);
    $block->save();
  }

  /**
   * Test custom block creation.
   */
  public function testRelatedContentBlockCreation() {
    $block = Block::load('custom_block_test');

    // Assert that the block exists.
    $this->assertInstanceOf(Block::class, $block);
    $this->assertEquals('related_contents_block', $block->getPluginId());

    // Render the block.
    $build = $this->entityTypeManager->getViewBuilder('block')->view($block);
    $this->assertNotEmpty($build, $this->t('Custom Block Test'));
  }

}
